# Terraform Examples
> Quick Examples for learning terraform


## Examples
- docker_container
  - demonstrates creating a container

- learn-terraform-aws-instance
  - demonstreates creating aws ec2 instance with terraform
  - terraform will output a `terraform.tfstate` file in json format
  - simple python script:
    ```
	import json
	
	# open the file
	with open('terraform.tfstate','r') as f:
		data = json.load(f)

	
	# print the contents of the state file
	print(data)

    ```





## Running
> TODO : update with terraform command information
```
# 
terraform init

# plan
terraform plan

# apply
terraform apply

# delete/destroy
terraform destroy

```

## Reference:
- https://learn.hashicorp.com/tutorials/terraform/
- https://learn.hashicorp.com/tutorials/terraform/aws-build

